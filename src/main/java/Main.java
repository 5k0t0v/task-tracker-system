import service.UIService;

public class Main {

    public static void main(String[] args) {
        UIService.getInstance().startApp();
    }
}