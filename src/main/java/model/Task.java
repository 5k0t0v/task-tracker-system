package model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private String title;
    private String description;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Task parent;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Project project;

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private boolean active;

    public Task(String title, String description, User user, Task parent, Project project, LocalDateTime startTime, LocalDateTime endTime, boolean active) {
        this.title = title;
        this.description = description;
        this.user = user;
        this.parent = parent;
        this.project = project;
        this.startTime = startTime;
        this.endTime = endTime;
        this.active = active;
    }

    public Task(String title, String description, Project project, boolean active) {
        this.title = title;
        this.description = description;
        this.project = project;
        this.active = active;
    }

    public Task() {
    }

    @Override
    public String toString() {
        return String.format("ID: %s | Title: %s | Description: %s",
                this.id, this.title, this.description);
    }
}
