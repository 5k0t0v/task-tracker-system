package service;

import lombok.Data;
import model.Project;
import model.Task;
import model.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Data
public class JsonParseService {
    private ArrayList<User> userList = new ArrayList<>();
    private ArrayList<Project> projectList = new ArrayList<>();
    private ArrayList<Task> taskList = new ArrayList<>();

    private static String getJsonFile() {
        Properties properties = new Properties();
        StringBuilder builder = new StringBuilder();
        try {
            properties.load(new FileInputStream("src/main/resources/application.properties"));
            String dataFile = properties.getProperty("initialDataFile");
            List<String> lines = Files.readAllLines(Paths.get(dataFile));
            lines.forEach(builder::append);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return builder.toString();
    }

    public void loadInitialData() throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject jsonData = (JSONObject) parser.parse(getJsonFile());

        JSONArray usersArray = (JSONArray) jsonData.get("users");
        parseUsers(usersArray);

        JSONArray projectsArray = (JSONArray) jsonData.get("projects");
        parseProjects(projectsArray);

        JSONArray tasksArray = (JSONArray) jsonData.get("tasks");
        parseTasks(tasksArray);

        for (User user : userList) {
            DbHandler.getInstance().addUser(user);
        }

        for (Project project : projectList) {
            DbHandler.getInstance().addProject(project);
        }

        for (Task task : taskList) {
           DbHandler.getInstance().addTask(task);
        }
    }

    private void parseUsers(JSONArray usersArray) {
        usersArray.forEach(userObject -> {
            JSONObject userJsonObject = (JSONObject) userObject;
            User user = new User((String) userJsonObject.get("name"));
            userList.add(user);
        });
    }

    private void parseProjects(JSONArray projectsArray) {
        projectsArray.forEach(projectObj -> {
            JSONObject projectJsonObject = (JSONObject) projectObj;
            Project project = new Project((String) projectJsonObject.get("title"),
                    (String) projectJsonObject.get("description"));
            projectList.add(project);
        });
    }

    private void parseTasks(JSONArray tasksArray) {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        tasksArray.forEach(taskObject -> {
            JSONObject taskJsonObject = (JSONObject) taskObject;
            Project project = null;
            for (Project element : projectList) {
                if (element.getTitle().equals(taskJsonObject.get("project"))) project = element;
            }
            Task task = new Task(
                    (String) taskJsonObject.get("title"),
                    (String) taskJsonObject.get("description"),
                    project,
                    (boolean) taskJsonObject.get("active"));
            taskList.add(task);
        });
    }
}
