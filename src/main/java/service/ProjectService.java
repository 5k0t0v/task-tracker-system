package service;

import lombok.Data;
import menu.Action;
import menu.Message;
import model.Project;
import model.User;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Scanner;

@Data
public class ProjectService {
    private static ProjectService instance;
    private List<Project> projectList;

    public static synchronized ProjectService getInstance() {
        if (instance == null)
            instance = new ProjectService();
        return instance;
    }

    public void printProjects() {
        System.out.println("Project list: ");
        List<Project> projects = getProjectList();
        if (projects.isEmpty()) System.out.println("\tproject list is empty!");
        for (Project project : projects) {
            System.out.println("\t" + project);
        }
    }

    public List<Project> getProjectList() {
        String hql = "FROM Project ";
        TypedQuery<Project> query = DbHandler.getInstance().getSession().createQuery(hql, Project.class);
        return query.getResultList();
    }

    public void deleteProject(Project project) {
        String hql = "delete from Project p where p.id = :id";
        Transaction transaction = DbHandler.getInstance().getSession().beginTransaction();
        Query query = DbHandler.getInstance().getSession().createQuery(hql);
        query.setParameter("id", project.getId());
        int result = query.executeUpdate();
        transaction.commit();
        System.out.println(Message.ROWS_AFFECTED + result);
    }

    private void deleteProject() {
        Project project = selectProject(Message.DELETE_PROJECT);
        deleteProject(project);
    }

    public void addProject() {
        Scanner in = new Scanner(System.in);
        Project project = new Project();

        System.out.print(Message.ENTER_TITLE);
        project.setTitle(in.nextLine().trim());

        System.out.print(Message.ENTER_DESCRIPTION);
        project.setDescription(in.nextLine().trim());
        project.setUser(UserService.getInstance().selectUser(Message.ASSIGN_USER));
        DbHandler.getInstance().addProject(project);
        System.out.println(Message.PROJECT_ADDED);
    }

    public Project selectProject(String title) {
        Project project = null;
        projectList = getProjectList();
        if (projectList.isEmpty()) {
            System.out.println(Message.PROJECT_LIST_IS_EMPTY);
            //selectProject();
            //projectList = getProjectList();
            return null;
        }
        Scanner in = new Scanner(System.in);
        int projectNumber = 1;
        System.out.println(title);
        for (Project element : projectList) {
            System.out.println("\t" + projectNumber++ + ") " + element.getTitle());
        }
        System.out.print(Message.ENTER_PROJECT_NUMBER);
        String projNum = in.nextLine().trim();
        try {
            int num = Integer.parseInt(projNum);
            if ((num > projectList.size()) || (num <= 0)) {
                System.out.println(Message.ERROR_NUMBER);
                return null;
            }
            project = projectList.get(num - 1);
        } catch (NumberFormatException ex) {
            System.out.println(Message.ASSIGN_SKIP);
        }
        return project;
    }

    public void editProject(int menuTab) {
        if (menuTab == Action.ADD) addProject();
        if (menuTab == Action.DEL) deleteProject();
        if (menuTab == Action.ASSIGN) assign();
    }

    public void assign() {
        Project project = selectProject(Message.SELECT_PROJECT);
        if (project == null) return;
        User user = UserService.getInstance().selectUser(Message.SELECT_USER);
        if (user == null) return;
        Transaction transaction = DbHandler.getInstance().getSession().beginTransaction();
        project.setUser(user);
        transaction.commit();
        System.out.println(Message.ASSIGN_PROJECT_EXECUTE);
    }

    public Project selectProjectByUser(User user) {
        projectList = getProjectList();
        for (Project proj : projectList) {
            if (proj.getUser().equals(user)) {
                return proj;
            }
        }
        return null;
    }

    public List<Project> getProjectListByUser(User user) {
        String hql = "FROM Project P where P.user.id = " + user.getId();
        TypedQuery<Project> query = DbHandler.getInstance().getSession().createQuery(hql, Project.class);
        return query.getResultList();
    }
}
