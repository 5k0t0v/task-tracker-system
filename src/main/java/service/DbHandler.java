package service;

import lombok.Data;
import model.Project;
import model.Task;
import model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Data
public class DbHandler {
    private Properties properties = new Properties();
    private static DbHandler instance = null;
    private Session session;
    private SessionFactory sessionFactory;

    public static synchronized DbHandler getInstance() {
        if (instance == null)
            instance = new DbHandler();
        return instance;
    }

    private DbHandler() {
        try {
            properties.load(new FileInputStream("src/main/resources/application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure(properties.getProperty("hibernateCfgFile")).build();
        Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
        this.sessionFactory = metadata.getSessionFactoryBuilder().build();
        this.session = sessionFactory.openSession();
    }

    public void addUser(User user) {
        Transaction transaction = session.beginTransaction();
        session.save(user);
        transaction.commit();
    }

    public void addProject(Project project) {
        Transaction transaction = session.beginTransaction();
        session.save(project);
        transaction.commit();
    }

    public void addTask(Task task) {
        Transaction transaction = session.beginTransaction();
        session.save(task);
        transaction.commit();
    }

    public void closeConnection() {
        sessionFactory.close();
    }
}
