package menu;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EditMenu {
    public void show(int menuId) {
        String cmd = switch (menuId) {
            case 1 -> "add";
            case 2 -> "delete";
            case 3 -> "assign";
            default -> "input error!";
        };
        System.out.println("EDIT DATA MENU:");
        System.out.println("\t1 - " + cmd + " user");
        System.out.println("\t2 - " + cmd + " project");
        System.out.println("\t3 - " + cmd + " task");
        System.out.println("\tl - load initial data");
        System.out.println("\tn - next");
        System.out.println("\tq - main menu");
        System.out.println("--");
        System.out.print(Message.ENTER_COMMAND);
    }
}
