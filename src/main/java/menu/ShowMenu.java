package menu;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ShowMenu {
    public void show() {
        System.out.println("VIEW DATA MENU:");
        System.out.println("\t1 - show a list of users");
        System.out.println("\t2 - show a list of projects");
        System.out.println("\t3 - show a list of tasks");
        System.out.println("\t4 - generate the report of all tasks created for specified projects by specified user");
        System.out.println("\tq - main menu");
        System.out.println("--");
        System.out.print(Message.ENTER_COMMAND);
    }
}
