package menu;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Action {
    public static final int ADD = 1;
    public static final int DEL = 2;
    public static final int ASSIGN = 3;
}
