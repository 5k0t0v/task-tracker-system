package menu;

import lombok.Data;

@Data
public class Message {
    public final static String ASSIGN_PROJECT = "assign project:";
    public final static String ASSIGN_PROJECT_EXECUTE = "\tassign project execute";
    public final static String ASSIGN_SKIP = "\tassign skipped";
    public final static String ASSIGN_TASK_EXECUTE = "\tassign task execute";
    public final static String ASSIGN_USER = "assign user:";
    public final static String ASSIGN_USER_EXECUTE = "\tassign user execute";
    public final static String DATABASE_IS_NOT_EMPTY = "\tdata base is not empty!";
    public final static String DELETE_USER = "delete user:";
    public final static String DELETE_PROJECT = "delete project:";
    public final static String ENTER_COMMAND = "enter a command: ";
    public final static String ENTER_DESCRIPTION = "enter a description: ";
    public final static String ENTER_PROJECT_NUMBER = "enter project number: ";
    public final static String ENTER_TASK_NUMBER = "enter task number: ";
    public final static String ENTER_TITLE = "enter a title: ";
    public final static String ENTER_USER_NAME = "enter the user name: ";
    public final static String ENTER_USER_NUMBER = "enter the user number: ";
    public final static String ERROR_NUMBER = "\terror number";
    public final static String EXIT_PROGRAM = "\tsession closed. bye!";
    public final static String INITIAL_DATA = "\tinitial data loaded";
    public final static String PROJECT_ADDED = "\tproject added";
    public final static String PROJECT_LIST_IS_EMPTY = "project list is empty";
    public final static String ROWS_AFFECTED = "rows affected: ";
    public final static String SELECT_PARENT_TASK = "select a parent task: ";
    public final static String SELECT_PROJECT = "select project: ";
    public final static String SELECT_TASK = "select task: ";
    public final static String SELECT_TASK_FOR_DELETE = "select task for delete: ";
    public final static String SELECT_USER = "select user: ";
    public final static String TASK_ADDED = "\ttask added";

    public final static String UNKNOWN_COMMAND = "\tunknown command";
    public final static String USER_ADDED = "\tuser added";
    public final static String USER_HAVE_NOT_PROJECTS = "\tuser have not projects";
    public final static String USER_LIST_EMPTY = "\tuser list is empty";
    public final static String USER_LIST = "user list: ";
}
