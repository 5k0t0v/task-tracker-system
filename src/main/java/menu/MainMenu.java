package menu;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MainMenu {
    public void show() {
        System.out.println("TaskTrackerSystem MAIN MENU:");
        System.out.println("\t1 - edit data");
        System.out.println("\t2 - view data");
        System.out.println("\tq - exit program");
        System.out.println("--");
        System.out.print(Message.ENTER_COMMAND);
    }
}
