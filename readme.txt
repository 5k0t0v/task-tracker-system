Task Tracker System

Load initial data
 - on the MAIN MENU press "1"
 - on the EDIT DATA MENU press "l" to load initial data
 - press q to MAIN MENU

Add, show and delete users, projects and tasks
 Add
  - user: on the MAIN MENU press "1" and on the EDIT DATA MENU press "1" 
  - project: on the MAIN MENU press  "1" and on the EDIT DATA MENU press "2"
  - task: on the MAIN MENU press "1" and on the EDIT DATA MENU press "3"
  - press q to MAIN MENU
 Show
  - user list: on the MAIN MENU press "2" and press "1"
  - project list: on the MAIN MENU press "2" and press "2"
  - task list: on the MAIN MENU press "1" and press "3"
  - press q to MAIN MENU
 Delete
  - user: on the MAIN MENU press "1", to next page press "n", press "1" to delete
  - project on the MAIN MENU press "1", to next page press "n", press "2" to delete
  - task on the MAIN MENU press "1", to next page press "n", press "3" to delete
  - press q to MAIN MENU

Assign user on the project
 - on the MAIN MENU press "1", to next page press "n", to next page press "n", press "1" to assign user on the project
 - press q to MAIN MENU

Assign task on the user
 - on the MAIN MENU press "1", to next page press "n", to next page press "n", press "3" to assign task on the user
 - press q to MAIN MENU

Generate the report of all tasks created for specified Projects by specified User
 - on the MAIN MENU press "2" 
 - on the VIEW DATA MENU press "4"
 - press q to MAIN MENU

